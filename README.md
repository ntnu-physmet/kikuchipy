 kikuchipy is an open-source Python library for processing and analysis of electron backscatter diffraction (EBSD) patterns.

 * User guide and documentation: https://kikuchipy.org
 * Code: https://github.com/pyxem/kikuchipy

---

Related scientific Python packages I (Håkon W. Ånes) and other NTNU employees
contribute too (some of which kikuchipy depends upon):
* HyperSpy: https://hyperspy.org
* orix: https://orix.readthedocs.io
* diffsims: https://diffsims.readthedocs.io
* pyxem: http://pyxem.org/
